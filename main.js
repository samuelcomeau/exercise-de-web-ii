//Texte
//Premier exemple
var pointage = 10;
console.log(pointage); //Pour afficher le pointage

console.log("Voici votre pointage : "+ pointage); //Concaténation possible
//On peut mettre du texte dans des ", ' `pour faire des sauts de lignes aussi
console.log("Voici votre \"pointage\" : "+ pointage); //Le caractère d'échappement est le \

//Deuxieme exemple

var nom = 'Comeau';
var prenom = 'Samuel';
console.log('Bonjour '+ prenom+' '+nom+',\nComment vas-tu?\nMoi, ça va super bien!');

//Autre façon d'intégrer les variables avec les accents graves pour les sauts de lignes
console.log(`Bonjour ${prenom} ${nom}, 
Comment ça va? 
Moi, ça va super bien!`)


//Calculs
//Premier exemple
var pointage = 10;
console.log(`Voici votre pointage ${pointage}`);
pointage+=10;
console.log(`voici votre pointage ${pointage}`);

var personne = { //On met ici un objet dans personne
    prenom: 'Samuel',
    nom: 'Comeau',
    lenny: function() {
        console.log('( ͡° ͜ʖ ͡°)');
    }
};

var personne = {

    prenom: 'Samuel',
    nom: 'Comeau',
    bonjour : function () {
        console.log('Bonjour! mon nom est ' + this.prenom + ' '+ this.nom)
    }
};

//5.3. Obtenir le type d'une donnée
typeof "Samuel" //retourne "string"
typeof 3.14 // retourne "number"
typeof true // retourne "boolean"
typeof x // retourne "undefined" (si x n'a pas été défini)

//5.4. Conversion
Number("3.14")// type number
String(12) // type string
Boolean("false") // type boolean

//Raccourcis
+"3.14" // type number
!!"" // type boolean (flase)

//5.5. Exercices
var race = "Golden retriever"
var sexe = "Femelle"
var prix = 4000
var ageEnSemaine = 6
var ageEnSemaineDisponible = 8

console.log(`${race} à vendre ${prix}$
Sexe : ${sexe}
Âge : ${ageEnSemaine} semaines(s)
Disponible dans ${ageEnSemaineDisponible-ageEnSemaine} semaines(s)`)

race = "Labrador"
sexe = "male"
ageEnSemaine = 4
ageEnSemaineDisponible = 7

console.log(`${race} à vendre ${prix}$
Sexe : ${sexe}
Âge : ${ageEnSemaine} semaines(s)
Disponible dans ${ageEnSemaineDisponible-ageEnSemaine} semaines(s)`)

//6. Conditions
//6.1. if, else et else if